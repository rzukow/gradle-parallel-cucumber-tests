Feature: Check Addition

  This scenarios checking that the addition works correctly


  Scenario: Add numbers to correct result 4
    Given x is set to 5
    And y is set to 2
    When the calculator adds x to y
    Then the result is 7

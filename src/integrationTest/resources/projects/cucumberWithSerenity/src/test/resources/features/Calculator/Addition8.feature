Feature: Check Addition

  This scenarios checking that the addition works correctly


  Scenario: Add numbers to correct result 8
    Given x is set to 12
    And y is set to 13
    When the calculator adds x to y
    Then the result is 25

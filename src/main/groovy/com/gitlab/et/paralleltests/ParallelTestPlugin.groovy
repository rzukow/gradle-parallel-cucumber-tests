package com.gitlab.et.paralleltests

import com.gitlab.et.paralleltests.helper.*
import com.gitlab.et.paralleltests.helper.model.Feature
import com.gitlab.et.paralleltests.helper.model.Test
import com.gitlab.et.paralleltests.helper.report.CucumberPluginResolver
import groovyx.gpars.GParsPool
import org.apache.commons.lang3.SystemUtils
import org.codehaus.groovy.runtime.ProcessGroovyMethods
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.JavaPluginConvention
import org.gradle.api.tasks.SourceSet

import static com.gitlab.et.paralleltests.helper.model.Result.FAILED
import static com.gitlab.et.paralleltests.helper.model.Result.PASSED

class ParallelTestPlugin implements Plugin<Project> {

    List<File> customClassPath = []
    List<Feature> featureFiles = []
    List<String> environmentVariables = []


    @Override
    void apply(Project project) {
        def extension = project.extensions.create('testParallel', ParallelTestExtension)

        project.task("overrideConfigByGivenParameters", dependsOn: ["compileJava", "compileTestJava"]) {
            doFirst {
                DynamicConfigLoader dcl = new DynamicConfigLoader(extension)
                extension = dcl.overrideByGivenParameters()
            }

        }

        project.task("validateConfiguration", dependsOn: "overrideConfigByGivenParameters") {
            doFirst {
                ExtensionValidator extensionValidator = new ExtensionValidator(extension)
                extensionValidator.checkMandatoryParametersArePresent()

                if (extension.printConfiguration) {
                    extensionValidator.printConfiguration()
                }

            }
        }

        project.task("locateFeatureFiles", dependsOn: "validateConfiguration") {
            doFirst {
                FeatureLoader featureLoader = new FeatureLoader(extension, project.file(".").getPath())
                featureFiles = featureLoader.locateFeatures()

                println "Found $featureFiles.size feature files"
                println "Found ${featureFiles.scenarios.flatten().size} scenarios"
                println "\n"

            }
        }

        project.task("provideClassPathVariable", dependsOn: "locateFeatureFiles") {
            doFirst {
                JavaPluginConvention javaConvention = project.getConvention().getPlugin(JavaPluginConvention.class)
                SourceSet main = javaConvention.getSourceSets().getByName(SourceSet.MAIN_SOURCE_SET_NAME)
                SourceSet test = javaConvention.getSourceSets().getByName(SourceSet.TEST_SOURCE_SET_NAME)
                main.runtimeClasspath.files.each { customClassPath.add it }
                test.runtimeClasspath.files.each { customClassPath.add it }
                customClassPath = customClassPath.unique()
                System.getenv().each() { k, v -> environmentVariables.add "$k=$v" }

                String delimiter = SystemUtils.IS_OS_WINDOWS ? ";" : ":"

                environmentVariables.add "CLASSPATH=${customClassPath.collect { it.getPath() }.join(delimiter)}"

            }

        }

        project.task("testParallel", dependsOn: "provideClassPathVariable") {

            group = 'Test parallel'
            description = 'runs cucumber tests in parallel'

            doLast {
                File workingDir = new File(".")
                CucumberPluginResolver cucumberPluginResolver = new CucumberPluginResolver()
                TestGenerator testgn = new TestGenerator(extension, cucumberPluginResolver)

                //Generate all tests
                List<Test> tests = []
                tests.addAll(testgn.generateTestsForFeatures(featureFiles))

                GParsPool.withPool(extension.parallelExecutions) {
                    tests.eachParallel { Test test ->
                        println("Start testing " + test.name)

                        OutputStream combinedOutputStream = new ByteArrayOutputStream()
                        long executionStartTime = System.currentTimeMillis()
                        Process proc = test.command.execute(environmentVariables, workingDir)
                        ProcessGroovyMethods.waitForProcessOutput(proc, combinedOutputStream, combinedOutputStream)
                        long executionEndTime = System.currentTimeMillis()
                        test.executionTimeInMilliSeconds = executionEndTime - executionStartTime

                        Boolean isPassed = proc.waitFor() == 0
                        if (!combinedOutputStream.toString().trim().startsWith("0 Scenarios")) {
                            if (isPassed) {
                                test.result = PASSED
                            } else {
                                test.result = FAILED
                            }
                        }


                        if (extension.printTestOutput) {
                            println(combinedOutputStream)
                        }

                    }
                }

                extension.cucumberPlugins.each { plugin ->
                    cucumberPluginResolver.mergeReportData(plugin)
                }
                FormattedPrinter.printTestResults(tests, System.err)
                if (tests.result.contains(FAILED)) {
                    throw new Exception("Not all tests passed")
                } else {
                    println "\nall tests passed"
                }

            }

        }
    }


}

package com.gitlab.et.paralleltests.helper.report

import org.apache.commons.lang3.RandomStringUtils

class CucumberJunitPlugin {
    List<String> testResultFileNames = new ArrayList<>()

    String generateAndAddRandomNameSuffix(String pluginParameter) {
        String testResultFileName = pluginParameter + randomNameSuffix()
        testResultFileNames << testResultFileName
        return testResultFileName

    }

    private static String randomNameSuffix() {
        int randomStringLength = 10
        String charset = (('a'..'z') + ('A'..'Z') + ('0'..'9')).join()
        String randomString = RandomStringUtils.random(randomStringLength, charset.toCharArray())
        String currentMillis = System.currentTimeMillis().toString()

        return "_" + currentMillis + "_" + randomString

    }


}

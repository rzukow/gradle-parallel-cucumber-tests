package com.gitlab.et.paralleltests.helper.report

class CucumberPluginResolver {

    CucumberJsonPlugin cucumberJsonPlugin = new CucumberJsonPlugin()
    CucumberJunitPlugin cucumberJunitPlugin = new CucumberJunitPlugin()


    public String pluginWithRandomizedFileName(String plugin) {
        String pluginCmdArgument

        if (plugin.startsWith("json")) {
            pluginCmdArgument = cucumberJsonPlugin.generateAndAddRandomNameSuffix(plugin)
        } else if (plugin.startsWith("junit")) {
            pluginCmdArgument = cucumberJunitPlugin.generateAndAddRandomNameSuffix(plugin)
        } else {
            pluginCmdArgument = plugin
        }

        return pluginCmdArgument
    }

    public mergeReportData(String plugin) {
        if (plugin.startsWith("json")) {
            cucumberJsonPlugin.mergeJsonData(plugin)
        } else if (plugin.startsWith("junit")) {
            // TODO: needs to be implemented
        } else {

        }


    }


}
